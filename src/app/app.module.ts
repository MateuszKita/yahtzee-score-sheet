import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {SharedModule} from "./shared/shared.module";
import {ScoreTableComponent} from './views/score-table/score-table.component';
import {NewGameDialogComponent} from './views/new-game-dialog/new-game-dialog.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    AppComponent,
    ScoreTableComponent,
    NewGameDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  providers: [],
  entryComponents: [NewGameDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
