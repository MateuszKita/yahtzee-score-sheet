import {Component, OnDestroy} from '@angular/core';
import {MatDialog} from "@angular/material";
import {NewGameDialogComponent} from "./views/new-game-dialog/new-game-dialog.component";
import {take, takeUntil} from "rxjs/operators";
import {Subject} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
  private onDestroy$: Subject<null> = new Subject();
  public title = 'Yahtzee Score Sheet';
  public playersList: string[];

  constructor(public readonly dialog: MatDialog) {
  }

  newGameClick(): void {
    const dialogRef = this.dialog.open(NewGameDialogComponent, {
      disableClose: true,
      width: '40vw',
      maxWidth: '400px',
      minWidth: '300px'
    });

    dialogRef.afterClosed()
      .pipe(
        take(1),
        takeUntil(this.onDestroy$)
      )
      .subscribe(result => {
        this.playersList = result;
      })
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
}
