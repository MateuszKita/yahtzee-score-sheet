import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {takeUntil} from "rxjs/operators";
import {Subject} from "rxjs";

@Component({
  selector: 'app-new-game-dialog',
  templateUrl: './new-game-dialog.component.html',
  styleUrls: ['./new-game-dialog.component.scss']
})
export class NewGameDialogComponent implements OnInit, OnDestroy {
  private onDestroy$: Subject<null> = new Subject();
  public form: FormGroup;
  public playersAmountOptions: number[] = [1, 2, 3, 4, 5];
  public selectedPlayersAmount: number = 0;
  public playersNames: string[] = [];

  constructor(
    public dialogRef: MatDialogRef<NewGameDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  ngOnInit(): void {
    this.buildForm();
    this.watchPlayersAmount()
  }

  private buildForm(): void {
    this.form = new FormGroup({
      playersAmount: new FormControl({
          value: 0, disabled: false
        },
        [Validators.required]
      ),
    });
  }

  private watchPlayersAmount(): void {
    this.form.controls.playersAmount.valueChanges
      .pipe(
        takeUntil(this.onDestroy$)
      )
      .subscribe(amount => {
        this.selectedPlayersAmount = amount;
        this.playersNames = [];
        for (let i = 0; i < amount; i++) {
          const playerName: string = `Player ${i + 1}`;
          this.playersNames.push(playerName);
          this.form.addControl(`player${i}`, new FormControl({
              value: playerName, disabled: false
            },
            [Validators.required, Validators.maxLength(10)]
          ));
        }
      })
  }

  createScoreSheet(): void {
    const playersToReturn: string[] = [];
    for (let i = 0; i < this.selectedPlayersAmount; i++) {
      playersToReturn.push(this.form.controls[`player${i}`].value);
    }
    this.dialogRef.close(playersToReturn);
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }


  ngOnDestroy(): void {
    this.onDestroy$.next();
  }
}
