import {TestBed} from '@angular/core/testing';

import {ScoreTableService} from './score-table.service';

describe('ScoreTableService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScoreTableService = TestBed.get(ScoreTableService);
    expect(service).toBeTruthy();
  });
});
